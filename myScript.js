/**
 * Created by Martin on 20.04.2017.
 */
(function () {
    'use strict';

    angular.module('app', []);
    angular.module('app').controller('C1', function () {
        this.myVar = 1;
        this.newItem = '';
        this.items = [];
        this.increase = function () {
            this.myVar++;
        };
        this.addItem = function () {
            this.newItem = {
                title: 'Hello', done: false
            };
            this.items.push(this.newItem);
            this.newItem = '';
        };
    });
})();